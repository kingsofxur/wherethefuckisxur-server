export default function TitanIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
      viewBox="0 0 32 32"
      {...props}
    >
      <path d="M14.839 15.979 1.661 8.37v15.218zm2.322 0 13.178 7.609V8.37zm5.485-12.175L16.057 0 2.879 7.609l13.178 7.609 13.179-7.609zm0 16.784-6.589-3.805-13.178 7.609L16.057 32l13.179-7.608-6.59-3.805z" />
    </svg>
  )
}