import { createClient } from "redis";

//This promise will run the first time it's awaited, then immediately return the result every time after
let redisClientPromise = new Promise<ReturnType<typeof createClient>>((resolve, reject) => {
	const client = createClient({
		url: 'redis://redis:6379'
	});
	client.connect().then(() => {
		resolve(client);
	})
});

export default async function getRedisClient() {
	return redisClientPromise;
}
