export function combineClasses(...classes: (string | undefined)[]) {
  return classes.filter(Boolean).join(" ");
}