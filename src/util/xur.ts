export function getXurLocationText(status: XurLocationStatus) {
  if (!status.present) {
    return "Xûr's fucked off";
  }
  return "He's just in the fuckin tower forever now, to the right of the ramen shop, whatever";
  //if (!status.location) {
  //  return "We're still looking, check back soon.";
  //}
  //const loc = status.location;
  //return loc ? `${loc.planet} > ${loc.zone} > ${loc.desc}` : "Xûr's fucked off";
}

export interface XurLocationStatus {
  present: boolean;
  lastUpdate?: number;
  location?: XurLocation;
}

export interface XurLocation {
  planet: string;
  zone: string;
  desc: string;
  customMap?: string;
  maps?: {
    featured: string;
    site: Record<string, string>;
    bot: Record<string, string>;
  };
}
