"use client";

import { createContext, Dispatch, SetStateAction, useEffect, useMemo, useState } from "react";

export const ThemeContext = createContext<
  [string, (newTheme: string) => void]
>(["match", (x: any) => { }]);

export default function Contexts({ children }: { children: React.ReactNode }) {
  const [theme, setTheme] = useState('match');

  useEffect(() => {
    console.log("useEffect triggered", localStorage.getItem('theme') ?? 'match');
    // Should just do this but can't while noflash isn't beforeInteractive
    // setTheme(localStorage.getItem('theme') ?? 'match');

    let storedTheme = localStorage.getItem('theme');
    console.log(storedTheme);
    if (storedTheme === null && !window.matchMedia('(prefers-color-scheme: dark)').matches) {
      console.log('no theme');
      document.documentElement.classList.remove('dark-theme');
      document.documentElement.classList.add('light-theme');
    } else if (storedTheme !== null) {
      console.log('theme');
      document.documentElement.classList.remove('dark-theme');
      document.documentElement.classList.add(storedTheme + '-theme');
      setTheme(storedTheme)
    }

    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
      console.log("system theme changed", theme);
      if (localStorage.getItem('theme') === null) {
        console.log("updating theme with system", theme, e.matches)
        if (e.matches) {
          document.documentElement.classList.remove('light-theme');
          document.documentElement.classList.add('dark-theme');
        } else {
          document.documentElement.classList.remove('dark-theme');
          document.documentElement.classList.add('light-theme');
        }
      }
    });
  }, [])

  function updateTheme(newTheme: string) {
    console.log("updating theme", theme, newTheme);
    setTheme(newTheme);
    if (theme !== 'match') {
      document.documentElement.classList.remove(theme + "-theme");
    } else {
      if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.documentElement.classList.remove('dark-theme');
      } else {
        document.documentElement.classList.remove('light-theme');
      }
    }
    if (newTheme !== 'match') {
      document.documentElement.classList.add(newTheme + "-theme");
      localStorage.setItem('theme', newTheme);
    } else {
      if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.documentElement.classList.add('dark-theme');
      } else {
        document.documentElement.classList.add('light-theme');
      }
      localStorage.removeItem('theme');
    }
  }

  return (
    <ThemeContext.Provider value={[theme, updateTheme]}>
      {children}
    </ThemeContext.Provider>
  );
}