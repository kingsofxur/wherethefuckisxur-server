import { TextBlock } from "@/components/text-block"
import Link from "next/link"

export default async function Contributors() {
    return (
        <>
            <h1 className="text-5xl font-bold mb-8">Contributors</h1>
            <TextBlock header="The Volunteers" className="mb-4">
                <p>{"Without our core volunteer team, the Xur Hunters, we would never be able to find Xur and quickly and accurately as we do now. And of course we have to mention all the people who have helped the server in some technical sense, from helping out to the code to just being a \"guy who knows stuff\" in the Discord. Without any of these volunteers, we wouldn't be where we are today."}</p>
            </TextBlock>
            <TextBlock header="The Dynamic Duo" className="mb-4">
                <p className="mb-4">At its core, WTFIX is an effort from two guys.</p>
                <ul className="list-disc list-inside ml-8 mb-4">
                    <li className=" ">NotDisliked, a <s>Computer Science student</s> professional software engineer who gets off on staring at hundreds of lines of code and spending hours trying to find the one semicolon he left out.</li>
                    <li className=" ">{"Dorkthrone, who's been running the site since the Destiny 1 days, and loves to come up with wacky wild high concept features and text blurbs to add to the site unrestricted."}</li>
                </ul>
                <p>{"We make this site in our free time, and it's very much a passion project for us. If you appreciate what we do, and would like to give back somehow, we only ask that you look at whatever charity we currently have listed on the homepage and consider donating to them.  We don't need the money, but they do."}</p>
            </TextBlock>
        </>
    )
}