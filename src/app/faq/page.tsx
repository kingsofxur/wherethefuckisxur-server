import { TextBlock } from "@/components/text-block"
import Link from "next/link"

export default async function FAQ() {
    const faqs = [{
        question: "Who The Fuck is Xûr?",
        answer: <p>{"Some tentacle face who got hired by the people who influence all of creation to rain exotics down on us like it's Christmas every Friday."}</p>
    }, {
        question: "Why the Fuck is Xûr?",
        answer: <p>{"Well little Timmy, when a mommy Xûr and a daddy Xûr love each other very very much..."}</p>
    }, {
        question: "Why the Fuck do you Swear so Much?",
        answer: <p>{"Oh, who the fuck knows. Settle down, Francis."}</p>
    }, {
        question: "Who the Fuck Makes this Site?",
        answer: <p>{"Dorkthrone. The P.T. Barnum of Xûr data services (you can quote me on that). It all started back in 2015 as a single page of good ol' fashioned pure static HTML that listed nothing and nothing but where the fuck Xûr was located. Then, out of nowhere, NotDisliked descended from the heavens, into the Discord server, with a single message: \"hi i can program a little bit\". Since then, there's been all kinds of contributors, in form of ideas and in code, and a volunteer team dedicated to hunting Xur down each and every Friday. And that's not even mentioning the "}<Link href="https://discord.gg/VE4vz5z">Discord</Link>{", the clan, and the wonderful community overall. We're amazed at what the site has become, and will keep posting Xur's location (and much more) for the rest of time."}</p>
    }, {
        question: "What the fuck is that little accent thingy over the \"u\" in Xûr's name?",
        answer: <p>{"It's called a circumflex and, yes, you should use it if you want to be down with the official lore."}</p>
    }]

    return (
        <>
            <h1 className="text-5xl font-bold mb-8">FAQ and Other Bullshit</h1>
            {faqs.map((x, i) => <TextBlock key={i} header={x.question} className="mb-4">
                {x.answer}
            </TextBlock>)}
        </>
    )
}
