export default async function PrivacyPolicy() {
  return (
    <>
      <h1 className="text-4xl font-bold text-[--special-text-color]">App</h1>
      <p>{"The WTFIX app does not collect any user data of any kind."}</p>
    </>
  );
}
