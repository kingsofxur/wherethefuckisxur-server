import { DataBlock } from "@/components/data-block"
import { TextBlock } from "@/components/text-block"
import Link from "next/link"

export default async function Friends() {
    return (
        <div className="max-w-xl m-auto">
            <h1 className="text-5xl font-bold mb-8">Contributors</h1>
            <DataBlock header="Friendgame" className="mb-4">
                <Link href="https://discord.gg/VE4vz5z" className="text-2xl">The Discord</Link>
                <p>Official WTFIX Discord</p>
            </DataBlock>
            <DataBlock header="Friends of Xur" className="mb-4">
                <Link href="https://twitch.tv/gladd" className="text-2xl">Gladd</Link>
                <p className="mb-4">He linked us so we link him. Thanks Gladd ❤</p>
                <Link href="https://bray.tech" className="text-2xl">Braytech</Link>
                <p>Dev friend of the site, and great resource for tracking yo shit. Looks fuckin gorgeous too.</p>
            </DataBlock>
        </div>
    )
}