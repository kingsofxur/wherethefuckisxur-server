import { TextBlock } from "@/components/text-block";
import Link from "next/link";

export default async function Bot() {
  return (
    <>
      <h1 className="text-4xl font-bold text-[--special-text-color]">The WTFIX Discord Bot</h1>
      <p>{"Want a handy bot in your Discord server that will post Xur's current location, with our Extremely Helpful™ Xur maps included for free? Now, you can add the official WTFIX Discord bot to your server, and get Xur's location via the built in \"/wtfix\" command, or by setting an automatic update channel and having the bot post xur's location automatically the moment we find him."}</p>
      <div className="my-8">
        <Link href="https://discord.com/api/oauth2/authorize?client_id=583853217062846484&permissions=18432&scope=bot%20applications.commands" className="text-6xl">Click here to add the bot to your server!</Link>
      </div>
      <TextBlock header="How The Fuck Do I Use This Thing" className="mt-8">
        <p>{"It's real simple, there are three commands (the default prefix is !, but that can be changed with the prefix command)."}</p>
        <ul className="list-disc list-inside ml-8 mb-4">
          <li>/wtfix: Post the current Xur location (or lack-there-of) with included Xur map!</li>
          <li>/setautochan (channelname): Change or set the auto post channel, the specified channel in the command will be where the bot posts the weekly location updates.</li>
          <li>/disableautochan: You know that channel that I talked about literally right above this line?  This command will make the bot forget it existed, so the auto-posts go away.</li>
        </ul>
      </TextBlock>
      <TextBlock header="Support" className="mt-8">
        If the bot is misbehaving, join our <Link href="https://discord.gg/VE4vz5z">Discord</Link> server and reach out in our #wtfix-support channel
      </TextBlock>
    </>
  )
}
