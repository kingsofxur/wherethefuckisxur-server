import ScrollDown from "@/components/scroll-down";
import { TextBlock } from "@/components/text-block";
import { ThemeSwitcher } from "@/components/theme-switcher";
import { XurTiming } from "@/components/xur-timing";
import { Flavor } from "@/util/flavor";
import { Metadata, Viewport } from "next";
import Image from "next/image";
import Link from "next/link";
import Script from "next/script";
import Contexts from "./contexts";
import "./globals.css";
import { getXurLocationText } from "@/util/xur";
import { getXurStatus } from "@/util/wtfixapi";

export const viewport: Viewport = {
  themeColor: "#DDDDDD",
  width: "device-width",
  initialScale: 1,
};

export async function generateMetadata(): Promise<Metadata> {
  let xurLocation: string | undefined = undefined;
  try {
    const xurStatus = await getXurStatus();
    xurLocation = getXurLocationText(xurStatus);
  } catch (e) {
    console.error(e);
  }
  const hostname = process.env["SERVER_HOSTNAME"];
  const base = hostname ? new URL(hostname) : undefined;
  return {
    title: "Where The Fuck Is Xur",
    description: "Xûr's location, updated each Friday.",
    keywords: [
      "xur",
      "destiny",
      "destinythegame",
      "whereisxur",
      "destiny 2",
      "D2",
    ],

    metadataBase: base,
    openGraph: {
      siteName: "Where The Fuck is Xûr?",
      title: "Where The Fuck is Xûr?",
      description: xurLocation,
      type: "website",
      url: "/",
      images: "/images/wtfix_background.png",
    },
  };
}

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const xurStatus = await getXurStatus();

  const logo = (
    <Link href="/">
      <Image
        src="/images/wtfix.png"
        alt="WTFIX"
        width={200}
        height={200}
        className="m-auto"
      />
    </Link>
  );
  const psa = (
    <TextBlock
      header="Public Xûrvice Announcement"
      headerClasses="text-4xl"
      className="w-full max-w-[35rem]"
    >
      <div
        className="flex flex-col gap-4 text-lg"
        dangerouslySetInnerHTML={{ __html: await Flavor.getPSA() }}
      ></div>
    </TextBlock>
  );
  const xur = (
    <>
      <h1 className="text-6xl font-bold text-[--special-text-color] md:text-8xl">
        {getXurLocationText(xurStatus)}
      </h1>
      <div className="text-3xl text-[--special-text-color]">
        <XurTiming lastUpdate={xurStatus.lastUpdate} />
      </div>
      <div
        className="text-2xl font-light italic"
        dangerouslySetInnerHTML={{ __html: await Flavor.getXurMsg() }}
      ></div>
    </>
  );

  return (
    <html
      lang="en"
      className="dark-theme theme-main-bg text-[--main-text-color]"
    >
      <head>
        <Script src="https://www.googletagmanager.com/gtag/js?id=UA-105724869-1"></Script>
        <Script id="google-tag-manager">{`
          window.dataLayer = window.dataLayer || [];
          function gtag() { dataLayer.push(arguments); }
          gtag('js', new Date());

          gtag('config', 'UA-105724869-1');
        `}</Script>
        <meta
          name="google-site-verification"
          content="OP9sOkRfEg6toierrIw22AmnW6vFFOfpmxcoeUvAiQE"
        ></meta>
        {/* Shouldn't this be beforeInteractive */}
        {/* <Script src="/noflash.js" /> */}
      </head>
      <body>
        <Contexts>
          <main>
            <div className="m-8 hidden justify-center gap-8 2xl:flex">
              <div className="flex-shrink-0">{logo}</div>
              <div className="max-w-5xl flex-1">{xur}</div>
              {psa}
            </div>
            <div className="m-8 hidden md:block 2xl:hidden">
              <div className="flex gap-8">
                <div className="flex-shrink-0">{logo}</div>
                {psa}
              </div>
              <div className="mt-8 max-w-full">{xur}</div>
            </div>
            <div className="m-8 flex flex-col items-center gap-8 md:hidden">
              <div className="max-w-[8rem]">{logo}</div>
              <div className="w-full max-w-[35rem]">{xur}</div>
              <div className="mt-5 w-full max-w-[35rem]">{psa}</div>
              <ScrollDown />
            </div>
            <div className="theme-block-bg flex w-full justify-center border-y border-[--border-color] py-3">
              <Link
                href="/"
                className="border-l border-r border-[--border-color] px-3 text-lg font-bold uppercase leading-tight text-[--main-text-color]"
              >
                Home
              </Link>
              <Link
                href="/bot"
                className="border-r border-[--border-color] px-3 text-lg font-bold uppercase leading-tight text-[--main-text-color]"
              >
                Bot
              </Link>
              <Link
                href="/app"
                className="border-r border-[--border-color] px-3 text-lg font-bold uppercase leading-tight text-[--main-text-color] fancy-text-anim"
              >
                The Future
              </Link>
              <div className="absolute right-2 -mt-1">
                <ThemeSwitcher />
              </div>
            </div>
            <div className="m-auto max-w-[90rem] p-8">{children}</div>
          </main>
          <footer>
            <div className="theme-secondary-bg border-t border-[--border-color] p-10">
              <div className="flex justify-center border-[--border-color] font-semibold uppercase tracking-wider">
                <Link
                  href="/faq"
                  className="border-l border-r border-[--border-color] px-2"
                >
                  FAQ
                </Link>
                {/* <p className="px-2 border-[--border-color] border-r">Archive</p> */}
                <Link
                  href="/friends"
                  className="border-r border-[--border-color] px-2"
                >
                  Friends
                </Link>
                <Link
                  href="/contributors"
                  className="border-r border-[--border-color] px-2"
                >
                  Contributors
                </Link>
              </div>
              <p className="mt-5 text-center">
                CONTACT: <Link href="https://discord.gg/VE4vz5z">Discord</Link>{" "}
                |{" "}
                <Link href="mailto:dork@wherethefuckisxur.com?Subject=Sup Dawg">
                  Email
                </Link>
              </p>
            </div>
            <div className="theme-footer-bg p-4 text-center text-xs text-[--footer-text-color]">
              <p>Not safe for ya mama since 2015.</p>
              <p>
                © Bungie, Inc. All rights reserved. Destiny, the Destiny Logo,
                Bungie and the Bungie logo are among the trademarks of Bungie,
                Inc.
              </p>
            </div>
          </footer>
        </Contexts>
      </body>
    </html>
  );
}
