import { combineClasses } from "@/util/css"

export function TextBlock({header, headerClasses, className, children}: {header: string, headerClasses?: string, className?: string, children: React.ReactNode}) {
  return (
    <div className={className}>
      <h2 className={combineClasses("text-[--special-text-color] text-3xl font-bold capitalize pb-2 border-b-2 border-[--line-border-color]", headerClasses)}>{header}</h2>
      <div className="mt-4">
        {children}
      </div>
    </div>
  )
}