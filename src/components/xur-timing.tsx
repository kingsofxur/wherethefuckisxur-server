"use client";

import isItXurTime from "@/util/xur-time";
import { useEffect, useState } from "react";

export function XurTiming({ lastUpdate }: { lastUpdate: number | undefined }) {
  const [currentTime, setCurrentTime] = useState<undefined | Date>(undefined);

  useEffect(() => {
    setCurrentTime(new Date());
    const countdownInterval = setInterval(() => {
      setCurrentTime(new Date());
    }, 1000);

    return () => {
      clearInterval(countdownInterval);
    }
  }, [])

  let text = "Xûr";

  if (!currentTime) {
    text += "..."
  } else {
    let lastFridayDayOfMonth: number;
    if (currentTime.getUTCDay() == 5) {
      lastFridayDayOfMonth = currentTime.getUTCDate();
    } else if (currentTime.getUTCDay() == 6) {
      lastFridayDayOfMonth = currentTime.getUTCDate() - 1;
    } else {
      lastFridayDayOfMonth = currentTime.getUTCDate() + (6 - currentTime.getUTCDay() - 1) - 7;
    }
    let xurFindTime = lastUpdate ? (new Date(lastUpdate).getTime() - new Date(Date.UTC(currentTime.getUTCFullYear(), currentTime.getUTCMonth(), lastFridayDayOfMonth, 17, 0, 0, 0)).getTime()) / 1000 : undefined;
  
    if (xurFindTime) {
      text += ` was found in ${xurFindTime} seconds, and`;
    }
  
    text += " will";
  
    let secondsUntilXurReset;
    if (isItXurTime()) {
      secondsUntilXurReset = nextResetOfDay(2).getTime() - currentTime.getTime();
      text += " leave";
    } else {
      secondsUntilXurReset = nextResetOfDay(5).getTime() - currentTime.getTime();
      text += " arrive";
    }
  
    text += ` in `;
  
    let secondsUntilReset = Math.floor(secondsUntilXurReset / 1000);
    const timeFragments = []
  
    if (secondsUntilReset > 86400) {
      const days = Math.floor(secondsUntilReset / 86400);
      secondsUntilReset -= days * 86400;
      timeFragments.push(`${days} days`);
    }
    if (secondsUntilReset > 3600) {
      const hours = Math.floor(secondsUntilReset / 3600);
      secondsUntilReset -= hours * 3600;
      timeFragments.push(`${hours} hours`);
    }
    if (secondsUntilReset > 60) {
      const minutes = Math.floor(secondsUntilReset / 60);
      secondsUntilReset -= minutes * 60;
      timeFragments.push(`${minutes} minutes`);
    }
    if (secondsUntilReset > 0) {
      timeFragments.push(`${secondsUntilReset} seconds`);
    }
  
    if (timeFragments.length > 1) {
      timeFragments[timeFragments.length - 1] = "and " + timeFragments[timeFragments.length - 1];
    }
  
    if (timeFragments.length > 2) {
      text += timeFragments.join(", ");
    } else {
      text += timeFragments.join(" ");
    }  
  }

  return (
    <>
      {<p>{text}</p>}
    </>
  )
}

function nextResetOfDay(dayOfWeek: number) {
  let nextXurReset = new Date();
  nextXurReset.setUTCDate(nextXurReset.getUTCDate() + (dayOfWeek+(7-nextXurReset.getUTCDay())) % 7);
  nextXurReset.setUTCHours(17, 0, 0, 0);
  return nextXurReset;
}