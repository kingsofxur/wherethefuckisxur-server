"use client";

import Image from 'next/image'
import { useEffect, useRef, useState } from 'react';

export default function ItemTooltip({ image, children }: { image: string, width?: number, height?: number, children?: React.ReactNode }) {
    const [hover, setHover] = useState(false);
    const [tapped, setTapped] = useState(false)
    const [[mouseX, mouseY], setMousePos] = useState([0, 0]);
    const tooltipRef = useRef(null)

    useEffect(() => {
        if (hover && !tapped) {
            const tooltip = tooltipRef.current as unknown as HTMLDivElement
            const tooltipRect = tooltip.getBoundingClientRect()
            const tooltipWidth = tooltipRect.width
            const tooltipHeight = tooltipRect.height
            const windowWidth = window.innerWidth
            const windowHeight = window.innerHeight
            const scrollY = window.scrollY
            let x = mouseX
            let y = mouseY + scrollY + 20
            if (x + tooltipWidth > windowWidth) {
                x = windowWidth - tooltipWidth
            }
            if (y + tooltipHeight > windowHeight + scrollY) {
                y = windowHeight - tooltipHeight + scrollY
            }
            tooltip.style.left = x + "px"
            tooltip.style.top = y + "px"
        }
    })

    return (<>
        <div className="relative">
            <Image src={image} alt="" height={200} width={200} onPointerEnter={(ev) => { ev.pointerType !== "touch" && setHover(true) }} onPointerLeave={(ev) => { ev.pointerType !== "touch" && setHover(false) }} onPointerMove={(ev) => { ev.pointerType !== "touch" && setMousePos([ev.clientX, ev.clientY]) }} onPointerDown={(ev) => {ev.pointerType === "mouse" && setTapped(true)}} onPointerUp={(ev) => {ev.pointerType === "touch" && setTapped(true)}}/>
        </div>
        {tapped && <div className="fixed z-50 bg-black bg-opacity-75 left-0 right-0 top-0 bottom-0 flex justify-center overflow-scroll" onClick={() => setTapped(false)}>
            <div className="m-auto py-10 pointer-events-none w-80 max-w-full h-fit drop-shadow-lg">{children}</div>
        </div>}
        {hover && !tapped && <div className="absolute z-50 pointer-events-none w-80 max-w-full" ref={tooltipRef}>{children}</div>}
    </>)
}