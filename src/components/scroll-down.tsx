"use client"
import { combineClasses } from '@/util/css';
import { Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/24/solid'

import { useEffect, useState } from "react";

export default function ScrollDown() {
    const [scrolled, setScrolled] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScrolled(window.scrollY > 0);
        });
    }, [])

    return (
        <Transition
            as="div"
            show={!scrolled}
            enter="transition-opacity duration-200"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
            className="fixed left-0 right-0 bottom-0 h-20 bg-gradient-to-t from-[--special-text-color] flex justify-center items-end">
            <ChevronDownIcon className="h-12 w-12 text-[--main-background-color] animate-bounce" onClick={() => window.scrollBy({ top: 250, behavior: "smooth" })} />
        </Transition>)
}