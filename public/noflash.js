; (function initTheme() {
  let theme = localStorage.getItem('theme');
  console.log(theme);
  if (theme === null && !window.matchMedia('(prefers-color-scheme: dark)').matches) {
    console.log('no theme');
    document.documentElement.classList.remove('dark-theme');
    document.documentElement.classList.add('light-theme');
  } else if (theme !== null) {
    console.log('theme');
    document.documentElement.classList.remove('dark-theme');
    document.documentElement.classList.add(theme + '-theme');
  }
})()