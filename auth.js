const { exit } = require('process');
const url = require('url');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

readline.question(`What's the app's client_id? `, client_id => {
    readline.question(`What's the app's auth secret? `, oauth_secret => {
        console.log(`Open URL: https://www.bungie.net/en/oauth/authorize?client_id=${client_id}&response_type=code&state=${oauth_secret}`);
        exit();
    })
})
